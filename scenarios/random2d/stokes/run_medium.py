import os
from contextlib import contextmanager
import subprocess
import numpy as np
import glob

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

def run(grid):

    problemName = grid.split('/')[-1]


    command = './stokes_random_2d -Grid.Image ' + grid
    command += ' > ' + problemName + '.log' + ' 2> ' + problemName + '.err '

    print(command)

    #subprocess.call(command, cwd=wd + '/' + problemName, shell=True)
    subprocess.call(command, shell=True)


if __name__ == "__main__":

    gridfiles = glob.glob("grids/20201209_Full_Dataset/Medium_grains/*pbm")
    idx = int(os.environ['SLURM_ARRAY_TASK_ID'])

    if (idx < len(gridfiles)):
        run(gridfiles[idx])
    else:
        print(idx, "is too high")
