// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Upscaling problem
 */
 #include <config.h>

 #include <ctime>
 #include <iostream>

 #include <dune/common/parallel/mpihelper.hh>
 #include <dune/common/timer.hh>
 #include <dune/grid/io/file/vtk.hh>
 #include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager_sub.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <filesystem>
#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>

#include "problem.hh"

template<class GridView>
void runPermeabilityUpscaling(const GridView& gridView)
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::StokesPermeabilityProblem;

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(gridView);
    gridGeometry->update();

    // channel bottom is at top of PM
    const auto channelBottomY = gridGeometry->bBoxMax()[1];

    // the problem (boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    const auto numDofsCellCenter = gridView.size(0);
    const auto numDofsFace = gridView.size(1);
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
    x[GridGeometry::faceIdx()].resize(numDofsFace);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using IOField = GetPropType<TypeTag, Properties::IOFields>;
    const auto gridPath = std::filesystem::path(std::string(std::filesystem::current_path()) + "/" + getParam<std::string>("Grid.Image"));

    // use the staggered FV assembler
    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);


    auto solveAndWriteOutput = [&](Problem::RunMode mode, double yChannelBottom)
    {
        problem->setRunMode(mode);
        problem->setChannelBottomY(yChannelBottom);

        const auto dir = (mode == Problem::RunMode::permeabilityX ? "x" : "y");

        const std::string name = getParam<std::string>("Problem.Name") + std::string(gridPath.filename())
                                 + "_permeability_" + dir;
        StaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, name);
        IOField::initOutputModule(vtkWriter); //!< Add model specific output fields
        vtkWriter.addField(problem->isInternalDirichlet(), "isInternalDirichlet");

        // set up two surfaces over which fluxes are calculated
        FluxOverSurface<GridVariables,
                        SolutionVector,
                        GetPropType<TypeTag, Properties::ModelTraits>,
                        GetPropType<TypeTag, Properties::LocalResidual>> flux(*gridVariables, x);

        using Scalar = GetPropType<TypeTag, Properties::Scalar>;

        const Scalar xMin = gridGeometry->bBoxMin()[0];
        const Scalar xMax = gridGeometry->bBoxMax()[0];
        const Scalar yMin = gridGeometry->bBoxMin()[1];
        const Scalar yMax = gridGeometry->bBoxMax()[1];

        using GlobalPosition = std::decay_t<decltype(gridGeometry->bBoxMax())>;

        if (mode == Problem::RunMode::permeabilityX)
        {
            // The second surface is placed at the inlet.
            const auto p0inlet = GlobalPosition{xMin, yMin};
            const auto p1inlet = GlobalPosition{xMin, yMax};
            flux.addSurface("inlet", p0inlet, p1inlet);

            // The second surface is placed at the outle.
            const auto p0outlet = GlobalPosition{xMax, yMin};
            const auto p1outlet = GlobalPosition{xMax, yMax};
            flux.addSurface("outlet", p0outlet, p1outlet);
        }
        else
        {
            // The second surface is placed at the inlet.
            const auto p0inlet = GlobalPosition{xMin, yMin};
            const auto p1inlet = GlobalPosition{xMax, yMin};
            flux.addSurface("inlet", p0inlet, p1inlet);

            // The second surface is placed at the outle.
            const auto p0outlet = GlobalPosition{xMin, yMax};
            const auto p1outlet = GlobalPosition{xMax, yMax};
            flux.addSurface("outlet", p0outlet, p1outlet);
        }

        // linearize & solve
        Dune::Timer assembleAndSolveTimer;

        nonLinearSolver.solve(x);

        std::cout << "Assembly and solve took " << assembleAndSolveTimer.elapsed() << " seconds" << std::endl;
        std::cout << "numFaceDofs: " << numDofsFace << ", numCellCenterDofs: " << numDofsCellCenter << ", total: " << numDofsFace + numDofsCellCenter << std::endl;

        // calculate and print mass fluxes over the planes
        flux.calculateMassOrMoleFluxes();
        const auto massFlux = flux.netFlux("outlet");

        static const Scalar rho = getParam<Scalar>("Component.LiquidDensity");
        static const Scalar nu = getParam<Scalar>("Component.LiquidKinematicViscosity");
        static const Scalar mu = rho * nu;
        static const Scalar pInlet = getParam<Scalar>("Problem.PressureInlet");

        // convert mass to volume flux
        const auto volumeFlux = massFlux / rho;

        const Scalar flowLength = (mode == Problem::RunMode::permeabilityX) ? xMax - xMin : yMax - yMin;
        const Scalar outflowArea = (mode == Problem::RunMode::permeabilityX) ? yMax - yMin : xMax - xMin;
        const Scalar gradP = pInlet / flowLength;

        const auto vDarcy = volumeFlux / outflowArea;
        const auto K = vDarcy / gradP * mu;

        // create temporary stringstream with fixed scientifc formatting without affecting std::cout
        std::ostream tmp(std::cout.rdbuf());
        tmp << std::fixed << std::scientific;
        tmp << "\n########################################\n" << std::endl;
        tmp << "dir " << dir << ": Area = " << outflowArea << " m^2";
        tmp << "; Massflux = " << massFlux << " kg/s";
        tmp << "; v_Darcy = " << vDarcy << " m/s";
        using std::abs;
        tmp << "; K = " << abs(K) << " m^2" << std::endl;
        tmp << "Flux inlet " << flux.netFlux("inlet") << std::endl;
        tmp << "Flux outlet - inlet: " << massFlux + flux.netFlux("inlet") << std::endl;
        tmp << "\n########################################\n" << std::endl;

        // write vtk output
        vtkWriter.write(0.0);
    };

    std::cout << "Calculating permeability in x-direction:" << std::endl;
    solveAndWriteOutput(Problem::RunMode::permeabilityX, channelBottomY);
    std::cout << "\n\nCalculating permeability in y-direction:" << std::endl;
    solveAndWriteOutput(Problem::RunMode::permeabilityY, channelBottomY);
}

template<class GridView>
void runCoupledFlow(const GridView& gridView, double channelBottomY)
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::StokesProblem;

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(gridView);
    gridGeometry->update();

    // the problem (boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    const auto numDofsCellCenter = gridView.size(0);
    const auto numDofsFace = gridView.size(1);
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
    x[GridGeometry::faceIdx()].resize(numDofsFace);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using IOField = GetPropType<TypeTag, Properties::IOFields>;
    const auto gridPath = std::filesystem::path(std::string(std::filesystem::current_path()) + "/" + getParam<std::string>("Grid.Image"));

    // use the staggered FV assembler
    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);


    auto solveAndWriteOutput = [&](Problem::RunMode mode, double yChannelBottom)
    {
        problem->setRunMode(mode);
        problem->setChannelBottomY(yChannelBottom);

        const std::string name = getParam<std::string>("Problem.Name") + std::string(gridPath.filename())
                                 + (mode == Problem::RunMode::coupledClosedWalls ? "_closedWalls" : "");
        StaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, name);
        IOField::initOutputModule(vtkWriter); //!< Add model specific output fields
        vtkWriter.addField(problem->isInternalDirichlet(), "isInternalDirichlet");

        // set up two surfaces over which fluxes are calculated
        FluxOverSurface<GridVariables,
                        SolutionVector,
                        GetPropType<TypeTag, Properties::ModelTraits>,
                        GetPropType<TypeTag, Properties::LocalResidual>> flux(*gridVariables, x);

        using Scalar = GetPropType<TypeTag, Properties::Scalar>;

        const Scalar xMin = gridGeometry->bBoxMin()[0];
        const Scalar xMax = gridGeometry->bBoxMax()[0];
        const Scalar yMin = gridGeometry->bBoxMin()[1];
        const Scalar yMax = gridGeometry->bBoxMax()[1];

        using GlobalPosition = std::decay_t<decltype(gridGeometry->bBoxMax())>;

        // total in
        const auto p0inlet = GlobalPosition{xMin, yMin};
        const auto p1inlet = GlobalPosition{xMin, yMax};
        flux.addSurface("totalIn", p0inlet, p1inlet);

        // total out
        const auto p0outlet = GlobalPosition{xMax, yMin};
        const auto p1outlet = GlobalPosition{xMax, yMax};
        flux.addSurface("totalOut", p0outlet, p1outlet);

        // channel in
        const auto p0ChannelIn = GlobalPosition{xMin, yChannelBottom};
        const auto p1ChannelIn = GlobalPosition{xMin, yMax};
        flux.addSurface("channelIn", p0ChannelIn, p1ChannelIn);

        // channel out
        const auto p0ChannelOut = GlobalPosition{xMax, yChannelBottom};
        const auto p1ChannelOut = GlobalPosition{xMax, yMax};
        flux.addSurface("channelOut", p0ChannelOut, p1ChannelOut);

        // PM in
        const auto p0PMIn = GlobalPosition{xMin, yMin};
        const auto p1PMIn = GlobalPosition{xMin, yChannelBottom};
        flux.addSurface("porousMediumIn", p0PMIn, p1PMIn);

        // PM out
        const auto p0PMOut = GlobalPosition{xMax, yMin};
        const auto p1PMOut = GlobalPosition{xMax, yChannelBottom};
        flux.addSurface("porousMediumOut", p0PMOut, p1PMOut);

        // interface
        const auto p0Interface = GlobalPosition{xMin, yChannelBottom};
        const auto p1Interface = GlobalPosition{xMax, yChannelBottom};
        flux.addSurface("interface", p0Interface, p1Interface);

        // linearize & solve
        Dune::Timer assembleAndSolveTimer;

        nonLinearSolver.solve(x);

        std::cout << "Assembly and solve took " << assembleAndSolveTimer.elapsed() << " seconds" << std::endl;
        std::cout << "numFaceDofs: " << numDofsFace << ", numCellCenterDofs: " << numDofsCellCenter << ", total: " << numDofsFace + numDofsCellCenter << std::endl;

        // calculate and print mass fluxes over the planes
        flux.calculateMassOrMoleFluxes();
        std::ostream tmp(std::cout.rdbuf());
        tmp << std::fixed << std::scientific;
        tmp << "totalIn: " << flux.netFlux("totalIn") << std::endl;
        tmp << "totalOut: " << flux.netFlux("totalOut") << std::endl;
        tmp << "channelIn: " << flux.netFlux("channelIn") << std::endl;
        tmp << "channelOut: " << flux.netFlux("channelOut") << std::endl;
        tmp << "porousMediumIn: " << flux.netFlux("porousMediumIn") << std::endl;
        tmp << "porousMediumOut: " << flux.netFlux("porousMediumOut") << std::endl;
        tmp << "interface: " << flux.netFlux("interface") << std::endl;

        tmp << "\n\n\nBalances:" << std::endl;
        tmp << "totalOut - totalIn: " << flux.netFlux("totalOut") + flux.netFlux("totalIn") << std::endl;
        tmp << "channelOut - channelIn: " << flux.netFlux("channelOut") + flux.netFlux("channelIn") << std::endl;
        tmp << "porousMediumOut - porousMediumIn: " << flux.netFlux("porousMediumOut") + flux.netFlux("porousMediumIn") << std::endl;

        // write vtk output
        vtkWriter.write(0.0);
    };

    std::cout << "\n\n***** Open Walls *******" << std::endl;
    solveAndWriteOutput(Problem::RunMode::coupledOpenWalls, channelBottomY);

    std::cout << "\n\n***** Closed Walls *******" << std::endl;
    solveAndWriteOutput(Problem::RunMode::coupledClosedWalls, channelBottomY);
}

int main(int argc, char** argv)
{
    using namespace Dumux;

    Dune::Timer timer;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::StokesProblem;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    using HostGrid = typename GetProp<TypeTag, Properties::Grid>::HostGrid;

    std::cout << "Constructing SubGrid from binary image" << std::endl;
    using GridManager = Dumux::GridManager<Dune::SubGrid<2, HostGrid>>;

    GridManager subgridManager; subgridManager.init();
    const auto& gridView = subgridManager.grid().leafGridView();

    // GridManager subgridManager; subgridManager.init();

    if (getParam<bool>("Problem.DebugGridOutput", false))
    {
        Dune::VTKWriter<GridManager::Grid::LeafGridView> duneVtkWriter(gridView);
        duneVtkWriter.write("subgrid_binary_image");
    }

    using SubSubGridManager = Dumux::GridManager<Dune::SubGrid<2, typename GridManager::Grid>>;
    // using SubSubGridManager = GridManager;
    using Element = typename HostGrid::LeafGridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using Scalar = typename GlobalPosition::value_type;

    Scalar yPosHighestGrainPixel = -1e10;
    const Scalar pixelSize = getParam<GlobalPosition>("Grid.PixelDimensions")[0];

    GlobalPosition bBoxMin = GlobalPosition{1e9, 1e9};
    GlobalPosition bBoxMax = GlobalPosition{-1e9, -1e9};

    // calculate the bounding box of the local partition of the grid view
    for (const auto& vertex : vertices(gridView))
    {
        for (int i=0; i<2; i++)
        {
            using std::min;
            using std::max;
            bBoxMin[i] = min(bBoxMin[i], vertex.geometry().corner(0)[i]);
            bBoxMax[i] = max(bBoxMax[i], vertex.geometry().corner(0)[i]);
        }
    }

    // find the y coordinate of the channel by identifying the uppermost grain pixel
    for (const auto& element : elements(gridView))
    {
        for (const auto& intersection : intersections(gridView, element))
        {
            if (!intersection.neighbor() && intersection.centerUnitOuterNormal()[1] < 0.0)
            {
                using std::max;
                yPosHighestGrainPixel = max(yPosHighestGrainPixel, intersection.geometry().center()[1]);
            }
        }
    }

    std::cout << "Free-flow channel starts at y " << yPosHighestGrainPixel << std::endl;

    const auto elementSelector = [&](const auto& element)
    {
        return element.geometry().center()[1] < yPosHighestGrainPixel + 0.3*pixelSize;
    };

    SubSubGridManager subSubGridManager; subSubGridManager.init(subgridManager.grid(), elementSelector);

    if (getParam<bool>("Problem.DebugGridOutput", false))
    {
        Dune::VTKWriter<SubSubGridManager::Grid::LeafGridView> duneVtkWriter(subSubGridManager.grid().leafGridView());
        duneVtkWriter.write("subgrid_binary_image_perm");
    }

    ////////////////////////////////////////////////////////////
    // run stationary linear problem on this grid
    ////////////////////////////////////////////////////////////

    runPermeabilityUpscaling(subSubGridManager.grid().leafGridView());

    runCoupledFlow(gridView, yPosHighestGrainPixel);

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
