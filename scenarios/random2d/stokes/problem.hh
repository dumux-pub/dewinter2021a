// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Channel flow test for the staggered grid (Navier-)Stokes model
 */
#ifndef DUMUX_CHANNEL_PROBLEM_HH
#define DUMUX_CHANNEL_PROBLEM_HH

#include <string>

#include <dune/grid/yaspgrid.hh>
#include <dumux/io/grid/gridmanager_sub.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>

namespace Dumux
{
template <class TypeTag>
class StokesProblem;

namespace Properties
{
// Create new type tags
namespace TTag {
struct StokesProblem { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
struct StokesPermeabilityProblem { using InheritsFrom = std::tuple<StokesProblem>; };

} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::StokesProblem>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar>>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::StokesProblem>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    static constexpr auto dim = 2;

public:
    using HostGrid = Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<Scalar, 2>>;
    using type = Dune::SubGrid<dim, HostGrid>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::StokesPermeabilityProblem>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    static constexpr auto dim = 2;
    using RealHostGrid = Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<Scalar, 2>>;
public:
    using HostGrid = Dune::SubGrid<dim, RealHostGrid>;
    using type = Dune::SubGrid<dim, HostGrid>;
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::StokesProblem> { using type = Dumux::StokesProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::StokesProblem> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::StokesProblem> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::StokesProblem> { static constexpr bool value = true; };

}

/*!
 * \brief  Test problem for the one-phase model:
   \todo doc me!
 */
template <class TypeTag>
class StokesProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;

    static constexpr int dimWorld = GridView::dimensionworld;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
    using Element = typename GridView::template Codim<0>::Entity;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

public:

    enum class RunMode { permeabilityX, permeabilityY, coupledOpenWalls, coupledClosedWalls };


    StokesProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry, std::to_string(dimWorld) + "D")
    {
        considerWallFriction_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.ConsiderWallFriction", false);
        if (considerWallFriction_)
            height_ = getParamFromGroup<Scalar>(this->paramGroup(), "Grid.Height");
        extrusionFactor_ = (considerWallFriction_ &&  GridView::dimensionworld == 2) ? 2.0/3.0 * height_ : 1.0;
        pInlet_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.PressureInlet");
        eps_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Epsilon", 1e-8);


        const auto elementsInLargestCluster = getElementsInLargestCluster_(gridGeometry->gridView());

        isInternalDirichlet_.resize(gridGeometry->gridView().size(0), true);
        for (const auto& element : elements(gridGeometry->gridView()))
        {
            const auto eIdx = gridGeometry->elementMapper().index(element);
            if (elementsInLargestCluster[eIdx])
                isInternalDirichlet_[eIdx] = false;
        }
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C


    const auto& isInternalDirichlet() const
    { return isInternalDirichlet_; }

    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    bool isDirichletCell(const Element& element,
                         const typename GridGeometry::LocalView& fvGeometry,
                         const typename GridGeometry::SubControlVolume& scv,
                         int pvIdx) const
    {
        return isInternalDirichlet_[scv.elementIndex()];
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     */
     BoundaryTypes boundaryTypes(const Element& element,
                                 const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        const auto& globalPos = scvf.dofPosition();
        const auto eIdx = this->gridGeometry().elementMapper().index(element);

        if (isInternalDirichlet_[eIdx])
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
        }
        else if (isInlet_(globalPos) || isOutlet_(globalPos))
            values.setDirichlet(Indices::pressureIdx);
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
        }

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);

        if (isInlet_(globalPos))
            values[Indices::pressureIdx] = pInlet_;

        return values;
    }

    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-control volume face.
     */
    using ParentType::source;
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const ElementFaceVariables& elemFaceVars,
                       const SubControlVolumeFace& scvf) const
    {
        auto source = NumEqVector(0.0);

#if DIM == 2 && CONSIDERWALLFRICTION
        if (GridView::dimensionworld == 2 && considerWallFriction_)
        {
            static const Scalar factor = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.PseudoWallFractionFactor", 8.0);
            source[scvf.directionIndex()] = this->pseudo3DWallFriction(scvf, elemVolVars, elemFaceVars, height_, factor);
        }
#endif

        return source;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        return PrimaryVariables(0.0);
    }

    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    { return extrusionFactor_; }

    void setRunMode(RunMode mode)
    { mode_ = mode; }

    void setChannelBottomY(const Scalar y)
    { channelBottomY_ = y; }

    // \}

private:

    bool isInlet_(const GlobalPosition& globalPos) const
    {
        if (mode_ == RunMode::permeabilityX || mode_ == RunMode::coupledOpenWalls)
        {
            // flow from left to right
            return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
        }
        else if (mode_ == RunMode::permeabilityY)
        {
            // flow from top to bottom
            return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_;
        }
        else
        {
            // flow from left to right, but closed lateral wall in PM
            return (globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_) && (globalPos[1] > channelBottomY_ - eps_);
        }
    }

    bool isOutlet_(const GlobalPosition& globalPos) const
    {
        if (mode_ == RunMode::permeabilityX || mode_ == RunMode::coupledOpenWalls)
        {
            // flow from left to right
            return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
        }
        else if (mode_ == RunMode::permeabilityY)
        {
            // flow from top to bottom
            return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_;
        }
        else
        {
            // flow from left to right, but closed lateral wall in PM
            return (globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_) && (globalPos[1] > channelBottomY_ - eps_);
        }
    }

    auto getElementsInLargestCluster_(const GridView& gridView)
    {
        const auto clusteredElements = clusterVoidElements_(gridView);
        const auto numVoidClusters = *std::max_element(clusteredElements.begin(), clusteredElements.end()) + 1;
        std::cout << "\nFound " << numVoidClusters << " void clusters." << std::endl;


        // count number of elements in individual clusters
        std::vector<std::size_t> voidClusterFrequency(numVoidClusters);
        for (const auto clusterIdx : clusteredElements)
            voidClusterFrequency[clusterIdx] += 1;

        const auto largestCluster = std::max_element(voidClusterFrequency.begin(), voidClusterFrequency.end());
        const auto largestClusterIdx = std::distance(voidClusterFrequency.begin(), largestCluster);

        std::vector<bool> elementsInLargestCluster(gridView.size(0));

        for (int eIdx = 0; eIdx < clusteredElements.size(); ++eIdx)
            if (clusteredElements[eIdx] == largestClusterIdx)
                elementsInLargestCluster[eIdx] = true;

        std::cout << "Largest cluster contains " << std::count(elementsInLargestCluster.begin(), elementsInLargestCluster.end(), true) << " elements" << std::endl;

        return elementsInLargestCluster;
    }

    /*!
     * \brief Assigns a cluster index for each void element.
     */
    std::vector<std::size_t> clusterVoidElements_(const GridView& gridView)
    {
        std::vector<std::size_t> elementInCluster(gridView.size(0), 0); // initially, all elements are in pseudo cluster 0
        std::size_t clusterIdx = 0;

        for (const auto& element : elements(gridView))
        {
            const auto eIdx = gridView.indexSet().index(element);
            if (elementInCluster[eIdx]) // element already is in a cluster
                continue;

            ++clusterIdx;
            elementInCluster[eIdx] = clusterIdx;

            recursivelyFindConnectedElements_(gridView, element, elementInCluster, clusterIdx);
        }

        // make sure the clusters start with index zero
        for (auto& idx : elementInCluster)
            idx -= 1;


        std::cout << "\nFound " << clusterIdx + 1 << " void clusters." << std::endl;

        return elementInCluster;
    }

    /*!
     * \brief Mark elements connected to a given element
     */
     template<class T>
     void recursivelyFindConnectedElements_(const GridView& gridView,
                                            const Element& element,
                                            std::vector<T>& elementIsConnected,
                                            const T marker = 1)
     {
         // use iteration instead of recursion here because the recursion can get too deep
         std::stack<Element> elementStack;
         elementStack.push(element);
         while (!elementStack.empty())
         {
             auto e = elementStack.top();
             elementStack.pop();
             for (const auto& intersection : intersections(gridView, e))
             {
                 if (!intersection.boundary())
                 {
                     auto outside = intersection.outside();
                     auto nIdx = gridView.indexSet().index(outside);
                     if (!elementIsConnected[nIdx])
                     {
                         elementIsConnected[nIdx] = marker;
                         elementStack.push(outside);
                     }
                 }
             }
         }
     }

    Scalar eps_;
    Scalar extrusionFactor_;
    Scalar pInlet_;
    bool calculateFluxesOverPlane_;
    Scalar height_;
    bool considerWallFriction_;
    Scalar channelBottomY_;
    RunMode mode_ = RunMode::coupledOpenWalls;
    std::vector<bool> isInternalDirichlet_;

};
} //end namespace

#endif
